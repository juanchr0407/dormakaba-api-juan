FROM node

WORKDIR /api

COPY . .
 
run npm install

EXPOSE 3000

CMD [ "npm", "start"]