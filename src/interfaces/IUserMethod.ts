import { Model } from "mongoose"; 
import { IUser } from "./IUser";

export interface IUserModel extends Model<IUser>{
    encryptPassword(password: string): string;
    comparePassword(password: string, receivedPassword: string): string;
}