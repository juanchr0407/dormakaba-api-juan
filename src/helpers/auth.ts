import { User } from "../models/User";
import {sign} from 'jsonwebtoken';
import config from "../config";

class Auth{
    
    private username: string;
    private password: string;

    constructor(username: string, password: string){
        this.username = username;
        this.password = password;
    }

    async register(){

        const userDB = await User.findOne({username: this.username});

        if (userDB) return 'ERR-D';

        const user = await new User({
            "username": this.username,
            "password": await User.encryptPassword(this.password)
        })

        const saveUser = await user.save();

        const jwt = sign({id: user.id}, config.jwtSecret as string, {
            expiresIn: '4h'
        })

        return jwt;
    }
    async login(){
        
        const userDB = await User.findOne({username: this.username});

        if (!userDB) return 'ERR-U';

        const validPassword = await User.comparePassword(this.password, userDB.password);

        if (!validPassword) return 'ERR-P';

        const jwt = sign({id: userDB.id}, config.jwtSecret as string, {
            expiresIn: '4h'
        })

        return jwt;
      
    }
}

export default Auth;