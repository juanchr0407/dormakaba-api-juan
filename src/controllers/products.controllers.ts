import { Request, Response } from "express"
import mongoose from "mongoose";
import { Product } from "../models/Product";
import { User } from "../models/User";

export const createProduct = async (req: Request, res: Response) => {
    
    try {
        
        const { title, description, image } = req.body;
        
        const product = await new Product({
            "title": title,
            "description": description,
            "image": image
        })
        const savedProduct = await product.save();

        res.status(201).json(savedProduct);

    } catch (error) {
        res.status(500).json({
            msg: 'Error 500 - Internal server error'
        })
    }
}
export const getProduct = async (req: Request, res: Response) => {

    try {
        
        const { id } = req.params;

        if ( !validateProductId(id) ) {
            return res.status(200).json({
                msg: 'No valid product.'
            })
        }
      
        const product = await Product.findById(id);
        return res.status(200).json(product);
        
    } catch (error) {
        res.status(500).json({
            msg: 'Error 500 - Internal server error'
        })
    }

}
export const getAllProducts = async (req: Request, res: Response) => {

    try {

        const products = await Product.find({});

        return res.status(200).json(products);
        
    } catch (error) {
        res.status(500).json({
            msg: 'Error 500 - Internal server error'
        })
    }

}
export const updateProduct = async (req: Request, res: Response) => {
    
    try {
        
        const { id } = req.params;
        const { title, description, image } = req.body;
        const updatedProduct = await Product.findByIdAndUpdate(id, { title, description, image }, { new: true });

        res.status(200).json(updatedProduct);

    } catch (error) {
        res.status(500).json({
            msg: 'Error 500 - Internal server error'
        })
    }
}
export const deleteProduct = async (req: Request, res: Response) => {
    
    try {
        
        const { id } = req.params;

        if ( !validateProductId(id) ) {
            return res.status(200).json({
                msg: 'No valid product.'
            })
        }

        const deletedProduct = await Product.findByIdAndDelete(id);
        res.status(200).json(deletedProduct);

    } catch (error) {
        res.status(500).json({
            msg: 'Error 500 - Internal server error'
        })
    }
}
export const toggleFav = async (req: Request, res: Response) => {
    try {
        
        const { productID, userID } = req.body;

        if ( !validateProductId(productID) || !validateProductId(userID)) {
            return res.status(200).json({
                msg: 'No valid product/user.'
            })
        }

        const favoriteProducts = await User.findById(userID).populate({
            path: "favorites",
        });

        if (!favoriteProducts || !favoriteProducts.favorites ) {
            return res.status(200).json({
                msg: 'No products found.'
            })
        }

        const { favorites } = favoriteProducts;

        var toggleFav = true;

        favorites.map((el) => {
            let id = new mongoose.Types.ObjectId(el['_id'])
            let idString = id.toString()
            if (idString === productID) toggleFav = false;
        });

        const savedUser = await User.findByIdAndUpdate(
            userID,
            ( toggleFav ) ? { $push: { favorites: productID } } : { $pull: { favorites: productID } },
            { new: true, useFindAndModify: false }
        );

        res.status(200).json(savedUser);

    } catch (error) {
        res.status(500).json({
            msg: 'Error 500 - Internal server error'
        })
    }
}
export const getFavorites = async (req: Request, res: Response) => {
    try {
        
        const { id } = req.params;

        if ( !validateProductId(id) ) {
            return res.status(200).json({
                msg: 'No valid user id.'
            })
        }

        const favoriteProducts = await User.findById(id).populate({
            path: "favorites",
        });

        return res.json(favoriteProducts);

    } catch (error) {
        res.status(500).json({
            msg: 'Error 500 - Internal server error'+ error
        })
    }
}
const validateProductId = (id:string) =>{
    return ( mongoose.Types.ObjectId.isValid(id) ) ? true:false;
}