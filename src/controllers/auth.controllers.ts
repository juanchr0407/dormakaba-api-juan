import { Request, Response } from "express";
import Auth from '../helpers/auth';

export const register = async (req: Request, res: Response) => {
    
    try {
        
        const { username, password } = req.body;
        
        const user = new Auth(username, password);
        const register = await user.register();

        if ( register === 'ERR-D'){
            //user exists
            return res.status(409).json({msg:"Username already exists."})
        }

        res.status(201).json({
            username, jwt: register
        });

    } catch (error) {
        res.status(500).json({
            msg: 'Error 500 - Internal server error'
        })
    }
}
export const login = async (req: Request, res: Response) => {
    try {

        const { username, password} = req.body;
        
        const user = new Auth(username, password);
        const login = await user.login();
        
        if (login === 'ERR-U') {
            res.status(400).json({
                msg: 'The username is not valid.'
            })            
        }
        else if (login === 'ERR-P') {
            res.status(400).json({
                msg: 'The password is not valid.'
            })         
        }else{
            res.status(201).json({ 
                msg: 'success',
                jwt: login 
            });
        }

    } catch (error) {
        res.status(500).json({
            msg: 'Error 500 - Internal server error.'
        })
    }
}

