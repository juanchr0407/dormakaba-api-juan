import { Schema, model } from 'mongoose';
import bcryptjs from 'bcryptjs'
import { IUser } from '../interfaces/IUser';
import { IUserModel } from '../interfaces/IUserMethod';

const UserSchema = new Schema({
    username:{
        type: String,
        required: [true, 'Username is required.'],
        unique:true
    },
    password:{
        type: String,
        required: [true, 'Password is required.'],
    },
    favorites:[{
        type: Schema.Types.ObjectId,
        ref:'Product'
    }]
})

UserSchema.static('encryptPassword', async(password: string)=>{
    const salt = await bcryptjs.genSalt(10);
    return await bcryptjs.hash(password, salt);
})

UserSchema.static('comparePassword', async(password:string, receivedPassword: string)=>{
    return await bcryptjs.compare(password, receivedPassword);
})
export const User = model<IUser, IUserModel>('User',UserSchema);