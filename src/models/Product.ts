import { Schema, model } from 'mongoose';
import { IProduct } from '../interfaces/IProduct';

const ProductSchema = new Schema({
    title:{
        type: String,
        required: [true, 'Product title is required.']
    },
    description:{
        type: String
    },
    image:{
        type: String
    },
    favorited:[{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }]
})

export const Product = model<IProduct>('Product',ProductSchema);