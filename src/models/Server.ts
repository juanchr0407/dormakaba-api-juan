import express, { Application } from 'express';
import cors from 'cors';
import config from '../config';

import { connectDB } from '../db/config';

import routerAuth from '../routes/auth.routes';
import routerProducts from '../routes/products.routes';
import routerError404 from '../routes/error404.routes';

class Server{
   
    private app: Application;
    private port: string;
    private path = {
        auth: '/api/v1/auth',
        products: '/api/v1/products',
        error404: '*'
    }

    constructor(){
        this.app = express();
        this.port = config.port as string;

        this.middlewares();
        this.routes();
        this.dbConnect();
    }

    async dbConnect(){
        await connectDB()
    }

    middlewares(){
        this.app.use(express.json());
        this.app.use(cors());
    }

    routes(){
        this.app.use(this.path.auth, routerAuth);
        this.app.use(this.path.products, routerProducts);
        this.app.use(this.path.error404, routerError404);
    }
    listen(){
        this.app.listen(this.port, () => {
            console.log(`Listening on port ${this.port}`);
        });
    }

}
export default Server;