import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import config from '../config';

export const validateToken = (req: Request, res: Response, next: NextFunction) => {

    try {
        
        const accessToken: any = req.header('authorization') || req.query.accessToken;

        if (!accessToken) {
            return res.status(400).json({
                msg: 'Access denied, you need a valid token to access this page.'
            })
        }

        jwt.verify(accessToken, config.jwtSecret as string)

        return next();

    } catch (error) {
        res.status(400).json({
            msg: 'Access denied, token expired or incorrect'
        })
    }
}