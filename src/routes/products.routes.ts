import { Router } from "express";
import { getProduct, getAllProducts, createProduct, updateProduct, deleteProduct, getFavorites, toggleFav } from "../controllers/products.controllers";
import { validateToken } from "../middlewares/validateJWT";

const router = Router();

router.get('/', validateToken, getAllProducts);
router.get('/:id', validateToken, getProduct);
router.get('/favorites/:id', validateToken, getFavorites);
router.post('/create', validateToken, createProduct);
router.patch('/:id', validateToken, updateProduct);
router.post('/fav', validateToken, toggleFav);
router.delete('/:id', validateToken, deleteProduct);

export default router;